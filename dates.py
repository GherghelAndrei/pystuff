# Rename files with American MM-DD-YYYY date format
# to European DD-MM-YYYY.

import shutil, re, os

date_pattern =  re.compile(r"""^(.*?) # all text before the date
 ((0|1)?\d)- # one or two digits for the month
 ((0|1|2|3)?\d)- # one or two digits for the day
 ((19|20)\d\d) # four digits for the year
 (.*?)$ # all text after the date
 """, re.VERBOSE)
for american_file in os.listdir('.'):
    mo = date_pattern.search(american_file)

    if mo == None:
        continue

    before_part = mo.group(1)
    month_part = mo.group(2)
    day_part = mo.group(4)
    year_part = mo.group(6)
    after_part = mo.group(8)

    # Form the european style file

    european_file = before_part + day_part + '-' + month_part + '-' + year_part + after_part

    abs_working_dir = os.path.abspath('.')
    american_file = os.path.join(abs_working_dir, american_file)
    european_file = os.path.join(abs_working_dir, european_file)

    #Rename the files

    print('Renaming "%s" to "%s"...' % (american_file, european_file))
    shutil.move(american_file, european_file)