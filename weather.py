# Prints the weather for a location from command line

import json, requests, sys

if len(sys.argv) < 2:
    print('Usage: quick.py locaion')
    sys.exit()

location = ' '.join(sys.argv[1:])

#Download json data from OpenWeatherMap api

url ='http://api.openweathermap.org/data/2.5/forecast/daily?q=%s&cnt=3'
response = requests.get(url)
response.raise_for_status()

weather_data = json.loads(response.text)

#Print weather descriptions

w = weather_data['list']
print('Current weather in %s: ' %(location))
print(w[0]['wearther'][0]['main'], '-',w[0]['weather'][0]['description'])
