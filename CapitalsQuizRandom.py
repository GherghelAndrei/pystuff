import random


capitals = {'Turcia': 'Ankara', 'Azerbaijan': 'Baku', 'Macedonia': 'Skopje',
            'Muntenegru': 'Podgorica', 'Italia': 'Roma', 'Ucraina': 'Kiev',
            'Spania': 'Madrid', 'Ungaria': 'Budapesta', 'Cehia': 'Praga',
            'Polonia': 'Varsovia', 'Letonia': 'Riga', 'Belarus': 'Minsk',
            'Slovenia': 'Ljubljana', 'Romania': 'Bucuresti'}

for capitalsKey in range(5):

    quizFile = open('CapitalsQuiz%s.txt' % (capitalsKey + 1), 'w')
    answerFile = open('AnswerQuiz%s.txt' % (capitalsKey + 1), 'w')

    quizFile.write('Name:\n\nDate:\n\n')
    quizFile.write((' ' * 20) + 'European Capitals Quiz Form%s' % (capitalsKey+1))
    quizFile.write('\n\n')

    country = list(capitals.keys())
    random.shuffle(country)

    for questionIndex in range(14):
        correctAnswer = capitals[country[questionIndex]]
        wrongAnswer = list(capitals.values())
        del wrongAnswer[wrongAnswer.index(correctAnswer)]
        wrongAnswer = random.sample(wrongAnswer, 3)
        answerOptions = wrongAnswer + [correctAnswer]
        random.shuffle(answerOptions)


        quizFile.write('%s. What is the capital of %s?\n' % (questionIndex + 1, country[questionIndex]))

        for i in range(4):
            quizFile.write('%s      %s\n' % ('ABCD'[i], answerOptions[i]))
            quizFile.write('\n\n')


        answerFile.write('%s. %s\n' % (questionIndex + 1, 'ABCD'[
                answerOptions.index(correctAnswer)]))

    quizFile.close()
    answerFile.close()